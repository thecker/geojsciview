const webpack = require('webpack');
const path = require('path');

module.exports = {
    // mode: 'development',
    entry: {
        main: './src/index.js',
        menu: './src/menu.js'
    },
    // devtool: 'inline-source-map',
    module: {
        rules: [
          {
            test: /\.css$/,
            loaders: ["style-loader","css-loader"]
          },
          {
            test: /\.(jpe?g|png|gif)$/i,
            loader:"file-loader",
            options:{
              name:'[name].[ext]',
              outputPath:'assets/images/'
              //the images will be emited to dist/assets/images/ folder
            }
          },
          {
            test: /\.js$/,
            loader: 'ify-loader'
          }
        ]
    },
    resolve: {
        modules: [path.resolve(__dirname, 'src'), 'node_modules']
    },
    plugins: [
        new webpack.ProvidePlugin({
          "$":"jquery",
          "jQuery":"jquery",
          "window.jQuery":"jquery",
          "window.$": "jquery"
        })
    ],
};
