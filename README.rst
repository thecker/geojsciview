geoJSciView
===========

*geoJSciView* is a browser based viewer for scienfitic data in geoJSON format.
It renders measurement results provided as geoJSON features in an interactive
3D view.

It provides easy configuration for the presented data via a JSON config file.
See 

You can see a live demo of *geoJSciView* at:
https://thecker.gitlab.io/geojsciview/demo/index.html

*geoJSciView* is free software released under the MIT license. It depends on 
following libraries:

* three.js (including examples)
* jQuery
* jQuery UI + themes (css)
* plotly
* webpack (for building the application)

It comes bundled with maps of Germany in geoJSON format (deutschlandGeoJSON).

For details on copyright and license information see LICENSE and NOTICE.

Getting started
---------------

The easiest way to build *geoJSciView* is to use *npm* (the package manager 
provided by *node.js*).
To get *npm* you will have to install node.js_ first.

Preparation & configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone the repository and install the required dependencies via *npm*.

.. code-block:: none

    git clone https://gitlab.com/thecker/geojsciview
    cd geojsciview
    npm install

You can now modify the configuration file in *src/config.json*.

The config file includes all setup options (e.g. source url of the geoJSON file,
titles to display, colorbar ranges,...) - see the example config also used for
the demo:

.. code-block:: JSON

    {
        "title": "Cs-137 concentration in waste, Germany",
        "init_pos": {
            "longitude": 11.5,
            "latitude": 53.0    
        },
        "colorbar": {
            "title": "Cs-137 concentration",
            "min_value": 0,
            "max_value": 200,
            "no_ticks": 4,
            "fontsize": 20
        },
        "options": {
	         "numeric_precision": 4,
	         "show_sample_number": true,
	         "show_marker": false,
	         "show_stats": false,
             "scale_factor": 0.01
        },
        "map": {
            "background_fill": {
                "filename": "static/data/maps/1_deutschland/4_niedrig.geojson",
                "color": "0xEEEEEE"
            },
            "contour_lines": {
                "filename": "static/data/maps/2_bundeslaender/4_niedrig.geojson",
                "color": "0xCCCCCC",
                "linewidth": 1
            }
        },
        "data": {
            "author": "BfS/IMIS geoportal",
            "author_link": "https://www.imis.bfs.de/geoportal/",
            "license": "dl-lb/by-2-0",
            "license_link": "http://www.govdata.de/dl-de/by-2-0",
            "source": "https://www.imis.bfs.de/ogc/opendata/ows...",
            "comment": "(adjusted visualization)",
            "convert_to_point": false,
            "value_name": "value",
            "unit": null
        }
    }

See config options for more details.

After having adjusted the config to your needs you can run *npm* to bundle the
application files.

.. code-block:: none

    npm run build

The bundled application files are then created in the *dist* folder.

Config options
--------------

The configuration file parameters are explained below.

title (string)
    
    Title text to be displayed in title bar

init_pos

    longitude (float)

        Longitude (in degree) for initial position of viewer

    latitude (float)

        Latitude (in degree) for initial position of viewer

colorbar

    title (string)
        
        Text to display for color bar legend title

    min_value (float)

        Measurement value to be used as lower end for color bar

    max_value (float)

        Measurement value to be used as upper end for color bar

    no_ticks (int)

        Number of ticks (incl. tick value) to display on color bar

    fontsize (int)

        Font size for color bar legend text (title and tick values)

options

    numeric_precision (int)

        Number of significant digits to display for measurement values

    show_sample_number (boolean)

        If true displays the number of samples above the measurement data

    show_marker (boolean)

        Display a red circle on the map around the selected data set 
        addtionally to the selection frame.

    show_stats (boolean)

        Display the three.js stats window (i.e. frame rate)

    scale_factor (float)

        Conversion factor for data value to height in the display - i.e. 
        conversion to scale in terms of degrees longitude/latitude

map

    background_fill

        Defines a GeoJSON file and options for a background fill shape (e.g. a GeoJSON definition of a country's borders)

            filename

                GeoJSON file name

            color

                Fill color used for display

    contour_lines

        Defines a GeoJSON file and options for contour lines (e.g. a GeoJSON definition of a country's federal states borders)

            filename

                GeoJSON file name

            color

                Line color used for display

            linewidth

                Width of the lines in pixels

data

    author (string)

        Name of author of the data (will be displayed in the bottom bar)

    author_link (string)

        Link to the author's homepage (hyperlink attached to author name)

    license (string)

        Name of the license for the data (will be displayed in the bottom bar)

    license_link (string)

        A link the license text (hyperlink attached to license name)

    source (string)

        Hyperlink to the data - will be used to load data and hyperlink will 
        be added to bottom bar

    comment (string)

        A comment on the data - will added to bottom bar

    conver_to_point (boolean)

        If true, features of geometry type "Polygon" or "Multipolygon" will be 
        displayed as a simple bar at the center point of the polygon. If false
        the polygon shape will be rendered.

    value_name (string)

        Name of the key, which stores the value for the measurement data in
        "properties".

    unit (string)

        Unit to be displayed for the data. If unit is null the program will
        look for a key "unit" in "properties" to get the unit.

Data format
-----------

The program expects the measurement data to be a geoJSON fiel with 
following properties:

* The data is stored inside a single "FeatureCollection"
* Following feature types are support: Point, MultiPoint, Polygon and 
  Multipolygon

Note, that MultiPoint and MultiPolygon are not displayed as linked data sets,
but as copies for each point or polygon inside the data.

Running test functions
----------------------

Test functions (using jest_) can be run via npm command line:

.. code-block:: none

    npm test -- --collectCoverage

`-- --collectCoverage` arguments can be omitted, if code coverage report is not desired.

.. _node.js: https://nodejs.org/en/
.. _jest: https://jestjs.io/

