import {create_meshes_for_all_data_points, create_mesh_for_data_point, create_annotation} from '../src/visual_objects';

import {Lut} from 'three/examples/jsm/math/Lut';

import * as THREE from 'three';

import {read_json} from '../src/geojson_parser';
import ConfigData from './config_test1.json';
import TestData from './test_data.json';

var lut = new Lut();
lut.setColorMap( 'cooltowarm' );
lut.setMax( 400 );
lut.setMin( 0 );

var point_data_set = {
    longitude: 10.5,
    latitude: 55.0,
    samples: 3,
    max_value: 200,
    min_value: 100,
    mean_value: 150,
    sample_properties: [
        {
            "unit": "kg/m3",
            "value": 100.0
        },
        {
            "unit": "kg/m3",
            "value": 150.0
        },
        {
            "unit": "kg/m3",
            "value": 200.0
        }
    ],
    point: true,
    center: {x: 10.5, y: 55.0},
    shape: null
};

var scale_factor = 5.0;

test('point based measurement data set created', () => {
    expect(create_mesh_for_data_point( point_data_set, scale_factor, lut )).toBeDefined();
    expect(create_mesh_for_data_point( point_data_set, scale_factor, lut, 0.01 ).position).toEqual(
        {"x": 10.5, "y": 0.01, "z": -55.0});
    expect(create_mesh_for_data_point( point_data_set, scale_factor, lut, 0.01, 0.1 ).scale).toEqual(
         {"x": 0.1, "y": 1000, "z": 0.1});
});

var position = new THREE.Vector3( 0, 1, 0)

// args: message, pos_vector, layer=1, scale=0.3
test('annotation sprite has been created', () => {
    expect(create_annotation( '1', position )).toBeDefined();
});

var parsed_data = read_json( TestData, ConfigData );

test('data mesh objects created from parsed GeoJSON data', () => {
    expect(create_meshes_for_all_data_points( parsed_data, lut, scale_factor )).toBeDefined();
    expect(create_meshes_for_all_data_points( parsed_data, lut, scale_factor ).meshes.length).toBe(7)
    expect(create_meshes_for_all_data_points( parsed_data, lut, scale_factor ).annotations.length).toBe(7)
});
