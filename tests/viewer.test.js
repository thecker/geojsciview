jest.mock('../src/webgl_scene');

import {GeoJSONViewer} from '../src/viewer';

var viewer = new GeoJSONViewer( '../tests/config_test1.json' );

const geoJSON_testdata = require('./test_data.json');
const map_data = require('./4_niedrig.json');

const event_update_to_min = { detail: {datatype: "min"}};
const event_update_to_max = { detail: {datatype: "max"}};
const event_update_to_mean = { detail: {datatype: "mean"}};

const event_show_annotations = { detail: {enabled: true}};
const event_hide_annotations = { detail: {enabled: false}};

var event_left_mouse_up = {button: 0, clientX: 125.0, clientY: 125.0, 
    preventDefault: function (){}};
var event_right_mouse_up = {button: 1, clientX: 0.0, clientY: 0.0, 
    preventDefault: function (){}};

// just run the program in the intended order of method calls
// asynchronous file loads have been replaced by direct calls with synchronous loading
it('viewer has been initialised', () => {
    viewer.load_config();
    viewer.add_scene();
    viewer.create_lut();
    viewer.add_colorbar();
    // file load functions
    viewer.create_data_visualizations(geoJSON_testdata);
    viewer.load_map_background(map_data);
    viewer.load_map_contour(map_data);
    
    expect(viewer.scene.scene).toBeDefined();
    expect(viewer.scene.scene.length).toEqual(54);
    expect(viewer).toHaveProperty('init_long', 11.5);
    expect(viewer).toHaveProperty('scene');
    
    // event triggered functions
    viewer.updateResults( event_update_to_min );
    viewer.updateResults( event_update_to_max );
    viewer.updateResults( event_update_to_mean );
    viewer.updateResults( null );
    viewer.showAnnotations( event_show_annotations );
    viewer.showAnnotations( event_hide_annotations );
    viewer.onWindowResize();
    viewer.onMouseUp( event_left_mouse_up );
    viewer.scene.raycaster.__setIntersectObject(viewer.mesh_lst[0]); // object to return send to mock
    viewer.render();

    expect(viewer.scene.mouse.x).toEqual(-0.5);
});
