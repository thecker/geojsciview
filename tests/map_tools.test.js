const load_map_geojson = require('../src/map_tools.js').load_map_geojson;

const map_data = require('./4_niedrig.json');

test('the map was loaded as a shape from GeoJSON file', () => {
    // args are GeoJSON data, color, line_width and boolean for fill (true) or contour (false)
    expect(load_map_geojson(map_data, 0xCCCCCC, 1, true).length).toBe(20);
    expect(load_map_geojson(map_data, 0xCCCCCC, 1, true)[0].updateMorphTargets).toBeDefined();
    expect(load_map_geojson(map_data, 0xCCCCCC, 1, true)[0].computeLineDistances).toBeUndefined();
});

test('the map was loaded as a contour from GeoJSON file', () => {
    // args are GeoJSON data, color, line_width and boolean for fill (true) or contour (false)
    expect(load_map_geojson(map_data, 0xCCCCCC, 1, false).length).toBe(20);
    expect(load_map_geojson(map_data, 0xCCCCCC, 1, false)[0].computeLineDistances).toBeDefined();
    expect(load_map_geojson(map_data, 0xCCCCCC, 1, false)[0].updateMorphTargets).toBeUndefined();
});

const map_data2 = require('./test_data.json');

test('the map was loaded as a contour from GeoJSON file', () => {
    // args are GeoJSON data, color, line_width and boolean for fill (true) or contour (false)
    expect(load_map_geojson(map_data2, 0xCCCCCC, 1, false).length).toBe(3);
    expect(load_map_geojson(map_data2, 0xCCCCCC, 1, false)[0].computeLineDistances).toBeDefined();
    expect(load_map_geojson(map_data2, 0xCCCCCC, 1, false)[0].updateMorphTargets).toBeUndefined();
});