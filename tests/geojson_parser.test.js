// import functions to test
const extract_features = require('../src/geojson_parser').extract_features;
const read_polygon = require('../src/geojson_parser').read_polygon;
const read_json = require('../src/geojson_parser').read_json;

// first list defines the outer shape coordinates and second the hole
const poly_coordinates_with_hole = [
                                     [
                                         [10.0, 52.0],
                                         [11.0, 52.0],
                                         [11.0, 53.0],
                                         [10.0, 53.0],
                                         [10.0, 52.0]
                                     ],
                                     [
                                         [10.2, 52.2],
                                         [10.8, 52.2],
                                         [10.8, 52.8],
                                         [10.2, 52.8],
                                         [10.2, 52.2]
                                     ]
];

const poly_properties = {value: null};

test('polygon object was created', () => {
    expect(read_polygon(
            {
                poly_coords: poly_coordinates_with_hole,
                poly_props: poly_properties,
                convert_to_point: false
            }).shape).toBeDefined();
    expect(read_polygon(
            {
                poly_coords: poly_coordinates_with_hole,
                poly_props: poly_properties,
                convert_to_point: false
            }).longitude).toStrictEqual([10.0, 11.0, 11.0, 10.0, 10.0]);
    expect(read_polygon(
            {
                poly_coords: poly_coordinates_with_hole,
                poly_props: poly_properties,
                convert_to_point: false
            }).latitude).toStrictEqual([52.0, 52.0, 53.0, 53.0, 52.0]);
    expect(read_polygon(
            {
                poly_coords: poly_coordinates_with_hole,
                poly_props: poly_properties,
                convert_to_point: false
            }).center).toStrictEqual({x: 10.5, y: 52.5});
});

test('polygon object was created with convert_to_point option', () => {
    expect(read_polygon(
            {
                poly_coords: poly_coordinates_with_hole,
                poly_props: poly_properties,
                convert_to_point: true
            }).shape).toBe(null);
    expect(read_polygon(
            {
                poly_coords: poly_coordinates_with_hole,
                poly_props: poly_properties,
                convert_to_point: true
            }).longitude).toBe(10.5);
    expect(read_polygon(
            {
                poly_coords: poly_coordinates_with_hole,
                poly_props: poly_properties,
                convert_to_point: true
            }).latitude).toBe(52.5);
    expect(read_polygon(
            {
                poly_coords: poly_coordinates_with_hole,
                poly_props: poly_properties,
                convert_to_point: true
            }).center).toStrictEqual({x: 10.5, y: 52.5});
});


// import JSON test data
const geo_json_data = require('./test_data.json');
const ConfigData = require('./config_test1.json');

test('the geo json features were extracted', () => {
    expect(extract_features(geo_json_data, ConfigData)).toBeDefined();
});

test('the unique locations and unit were extracted', () => {
    expect(read_json(geo_json_data, ConfigData).unit).toBe('kg/m3');
    expect(read_json(geo_json_data, ConfigData).unique_locs.length).toBe(7);
});

const ConfigData2 = require('./config_test2.json');

test('the unique locations and unit were extracted - with user defined unit', () => {
    expect(read_json(geo_json_data, ConfigData2).unit).toBe('m/s');
});

