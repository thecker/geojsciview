const setLegendOn = require('../src/colorbar.js').setLegendOn;
import {Lut} from 'three/examples/jsm/math/Lut';
// const Lut = require('three/examples/jsm/math/Lut');

var lut = new Lut();
lut.setColorMap( 'cooltowarm' );
lut.setMax( 100 );
lut.setMin( 0 );

test('color bar object is created', () => {
    expect(setLegendOn( lut )).toBeDefined();
});