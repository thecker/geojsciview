import {create_samples_summary_string, create_histogram_props, create_location_props_string} from '../src/sample_info';

var point_data_set = {
    longitude: 10.5,
    latitude: 55.0,
    samples: 3,
    max_value: 300,
    min_value: 100,
    mean_value: 200,
    sample_properties: [
        {
            "value": 100.0,
            "unit": "kg/m3"
        },
        {
            "value": 200.0,
            "unit": "kg/m3"
        },
        {
            "value": 300.0,
            "unit": "kg/m3"
        }
    ],
    point: true,
    center: {x: 10.5, y: 55.0},
    shape: null
};

test('sample info summary created', () => {
    expect(create_samples_summary_string( point_data_set.sample_properties )).toBe("<h3>Sample 1/3</h3><div><p>value: 100<br>unit: kg/m3<br></p></div><h3>Sample 2/3</h3><div><p>value: 200<br>unit: kg/m3<br></p></div><h3>Sample 3/3</h3><div><p>value: 300<br>unit: kg/m3<br></p></div>"
)
});

test('histogram props created', () => {
    expect(create_histogram_props( point_data_set.sample_properties, 0, 500, 'm/s2' ).curves[0].x).toEqual([100, 200, 300]);
    expect(create_histogram_props( point_data_set.sample_properties, 0, 500, 'm/s2' ).curves[0].xbins.start).toEqual(0);
    expect(create_histogram_props( point_data_set.sample_properties, 0, 500, 'm/s2' ).curves[0].xbins.end).toEqual(500);
    expect(create_histogram_props( point_data_set.sample_properties, 0, 500, 'm/s2' ).curves[0].xbins.size).toEqual(50);
});

test('sample location info created', () => {
    expect(create_location_props_string( point_data_set, 4, 'm/s2' )).toBe("<span>longitude: </span><span>10.5 [°]</span><span>latitude: </span><span>55 [°]</span><span>samples: </span><span>3</span><span>max. value: </span><span>300 [m/s2]</span><span>min. value: </span><span>100 [m/s2]</span><span>mean value: </span><span>200 [m/s2]</span>"
);
});
