/*
This file is part of geoJSciView

Copyright 2020 Thies Hecker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

import * as THREE from 'three';

/**
 * Creates the 3D representations of all measurement samples
 * @param {Object} parsed_data - Parsed GeoJSON data - see geojson_parser.read_json
 * @returns {object} - a dictionary with meshes (array) and annotations (array)
 * @param {object} lut - three.js Lut (Look-Up-Table) object for color scale
 */
export function create_meshes_for_all_data_points(parsed_data, lut, scale_factor) {
    var annotation; // annotation to display the samples per measurement
    var annotation_lst = [];

    var mesh;
    var mesh_lst = [];

    // var parsed_data = read_json(json, ConfigData);
    var unique_locs = parsed_data.unique_locs;
    var unit = parsed_data.unit;

    // create graphis objects
    for (const data_set of unique_locs){
        mesh = create_mesh_for_data_point( data_set, scale_factor, lut );
        mesh_lst.push(mesh);
        annotation = create_annotation(data_set.samples.toString(), new THREE.Vector3(data_set.center.x,
        		    data_set.max_value / 100 + 0.1,  -data_set.center.y), 1);
        annotation_lst.push(annotation);
    }
    return {meshes: mesh_lst, annotations: annotation_lst}
}


/**
 * Creates the 3D representations of a measurement samples
 * @param {Object} data_set - Data container for a unique location measurement collection
 * @param {float} scale_factor - Factor to use for height scaling of the data representation
 * @param {object} lut - three.js Lut (Look-Up-Table) object for color scale
 * @param {float} y_offset - Offset in height direction (will make the mesh hover over ground if positive)
 * @param {float} scale_xz - mesh dimension in longitude/latitude direction (only applicable to point representation)
 */
export function create_mesh_for_data_point(data_set, scale_factor, lut, y_offset=0.01, scale_xz=0.1) {

    var mesh;

    var color = lut.getColor( data_set.max_value );
    // use MeshBasicMaterial without light source to assure match to color bar
    var material = new THREE.MeshBasicMaterial( {
         color: color, wireframe: false, transparent: true, opacity: 1.0
         } );

    if (data_set.point){
        mesh = create_box_mesh(material, data_set.center.x, data_set.center.y,
                               data_set.max_value * scale_factor, scale_xz, y_offset);
    }else{	// this is data is a polygon...
        mesh = create_polygon_mesh(material, data_set.shape, data_set.max_value * scale_factor, y_offset)
    }

    mesh.updateMatrix();
    mesh.matrixAutoUpdate = false;
    // scene.add( mesh );
    return mesh;
}


/**
 * creates a simple box mesh
 * @param {object} material - three.js material object
 * @param {float} center_x - longitude coordinate
 * @param {float} center_y - latitude coordinate
 * @param {float} height - height (in longitude/latitude units)
 * @param {float} scale_xz - mesh dimension in longitude/latitude direction
 * @param {float} y_offset - Offset in height direction (will make the mesh hover over ground if positive)
 */
function create_box_mesh ( material, center_x, center_y, height, scale_xz, y_offset ) {

    var geometry = new THREE.BoxBufferGeometry( 1, 1, 1 );
    geometry.translate( 0, 0.5, 0 );

    var mesh = new THREE.Mesh( geometry, material );
    mesh.position.x = center_x;
    mesh.position.z = -center_y;
    mesh.scale.x = scale_xz;
    mesh.scale.z = scale_xz;
    mesh.position.y = y_offset;
    mesh.scale.y = height;

    return mesh;
}

/**
 * creates an extruded polygon mesh
 * @param {object} material - three.js material object
 * @param {object} shape - three.js shape object
 * @param {float} height - height (in longitude/latitude units)
 * @param {float} y_offset - Offset in height direction (will make the mesh hover over ground if positive)
 */
function create_polygon_mesh ( material, shape, height, y_offset) {

    var extrudeSettings = {
        steps: 2,
        depth: 1,
        bevelEnabled: false,
    };
    var shape_geo = new THREE.ExtrudeBufferGeometry( shape, extrudeSettings );
    var mesh = new THREE.Mesh( shape_geo, material );
    mesh.scale.z = height;
    mesh.translateY(mesh.position.z + y_offset);
    mesh.rotateX(-Math.PI/2);

    return mesh;
}

/**
 * adds annotation as sprites (e.g. number samples per measurement)
 * @param {string} message - Annotation content
 * @param {object} pos_vector - three.js Vector3D for position of the annotation
 * @param {int} layer - layer number to which the annotation shall be added
 * @param {float} scale - scale of the annotation
 */
export function create_annotation(message, pos_vector, layer=1, scale=0.3){

	// console.log(message);

	var sprite = true;

	// Number
	var canvas = document.createElement("canvas");
	canvas.id = "number" + message;
	canvas.width = 64;
	canvas.height = 64;

	//var canvas = document.getElementById("number");
	var ctx = canvas.getContext("2d");
	const x = 32;
	const y = 32;

	ctx.fillStyle = "rgba(0, 0, 0, 1.0)";
	ctx.font = "32px sans-serif";
	ctx.textAlign = "center";
	ctx.textBaseline = "middle";
	var size_ratio = Math.ceil((ctx.measureText(message).width/canvas.width))
	if (size_ratio > 1){
		canvas.width = size_ratio * 64;
		//canvas.height = size_ratio * 64;
	}
	ctx.fillText(message, canvas.width/2, canvas.height/2);

	// console.log(canvas.width);

	document.body.appendChild(canvas);

	// Sprite
	var numberTexture = new THREE.CanvasTexture(document.querySelector("#number" + message));

   var spriteMaterial = new THREE.SpriteMaterial({
       map: numberTexture,
   });

   var sprite = new THREE.Sprite(spriteMaterial);
   sprite.position.set(pos_vector.x, pos_vector.y, pos_vector.z);
   sprite.scale.set(scale, scale, scale);

   sprite.layers.set(layer);
   // console.log('Sprite', sprite, 'added.');

   return sprite;
}

//module.exports = {
//    create_meshes_for_all_data_points: create_meshes_for_all_data_points,
//    create_mesh_for_data_point: create_mesh_for_data_point,
//    create_annotation: create_annotation
//};
