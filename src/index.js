/*
This file is part of geoJSciView

Copyright 2020 Thies Hecker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


A lot of code based on the three.js examples - see license of three.js (r113) below:

The MIT License

Copyright © 2010-2020 three.js authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


The "add_annotations" function has been inspired by Manuel Wieser's 
"WebGL Annotations (three.js)" codepen.

Copyright (c) 2020 by Manuel Wieser (https://codepen.io/Lorti/pen/Vbppap)

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.
*/

import {GeoJSONViewer} from './viewer';

// init();

var viewer = new GeoJSONViewer( 'config.json' );

viewer.load_config();
viewer.add_scene();
viewer.create_lut();
viewer.add_colorbar();

load_data();

// viewer.render();
// viewer.animate();

window.requestAnimationFrame( render );

window.addEventListener( 'resize', onWindowResize, false );
window.addEventListener( 'mousedown', onMouseDown, false );
window.addEventListener( 'mouseup', onMouseUp, false );
// custom events send from menu.js
window.addEventListener( 'datatypechange', update_results, false );
window.addEventListener( 'annotationstoggle', show_annotations, false );

animate();
// viewer.render();
// viewer.animate();


function render(){
    viewer.render();
}

function animate(){
    requestAnimationFrame( animate );
    viewer.animate();
}

function update_results( event ){
    viewer.updateResults( event );
}

function show_annotations( event ){
    viewer.showAnnotations( event );
}

function onWindowResize ( event ){
    viewer.onWindowResize( event );
}

function onMouseDown( event ){
    viewer.onMouseDown();
}

function onMouseUp( event ){
    viewer.onMouseUp( event );
}

function load_data( ){
    // load sample data
    $.getJSON(viewer.ConfigData.data.source, load_sample_data);

    // load map from JSON file
    $.getJSON(viewer.ConfigData.map.contour_lines.filename, load_map_contour);
    $.getJSON(viewer.ConfigData.map.background_fill.filename, load_map_background);
}

function load_sample_data( data ){
    viewer.create_data_visualizations( data );
}

function load_map_contour( data ){
    viewer.load_map_contour( data );
}

function load_map_background( data ){
    viewer.load_map_background( data );
}


/**
 * initializes the program (loads maps and data, calls scene setup)
 */
function init() {
    
    var viewer = new GeoJSONViewer( 'config.json' );
    
    viewer.load_config();
    viewer.add_scene();
    viewer.create_lut();
    viewer.add_colorbar();
    
    // viewer.load_data();

    // viewer.render();
    viewer.animate();

    // event listeners
    /*
    window.addEventListener( 'resize', onWindowResize, false );
    mousedown_active = false;
    window.addEventListener( 'mousedown', onMouseDown, false);
    window.addEventListener( 'mouseup', onMouseUp, false );
    // window.addEventListener( 'mouseup', onMouseUp, false );
    // custom events send from menu.js
    window.addEventListener( 'datatypechange', updateResults, false);
    window.addEventListener( 'annotationstoggle', showAnnotations, false );
    */
}
