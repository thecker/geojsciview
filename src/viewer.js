/*
This file is part of geoJSciView

Copyright 2020 Thies Hecker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// import Plotly from 'plotly.js-dist';
// TODO: see https://github.com/plotly/plotly.js/issues/3518 - 
// currenlty uses CDN version instead (see dist/index.html)

var $ = require('jquery');

import * as THREE from 'three';

import {WebGLScene} from 'webgl_scene';

import {setLegendOn, setLegendLabels} from './colorbar.js';

import {read_json} from './geojson_parser';

import {load_map_geojson} from './map_tools.js';

import {create_meshes_for_all_data_points} from './visual_objects';

import {create_histogram_props, create_samples_summary_string, 
    create_location_props_string} from './sample_info';

import {Lut} from 'three/examples/jsm/math/Lut';

import ConfigData from './config.json';


/**
 * Class for Viewer
 * @param {string} config_file - Name of config JSON file
 */
export class GeoJSONViewer {

    constructor( config_file ){
        this.config_file = config_file;
        this.ConfigData = null;

        // config attributes
        this.init_long = null;
        this.init_lat = null;

        this.value_min = null;
        this.value_max = null;
        this.unit = null;

        this.title_text = null;
        this.colorbar_title = null;
        this.data_source = null;

        // scene attributes
        this.scene = null;
        this.lut = null;
        this.colobar_labels = null;

        this.mesh_lst = [];
        this.annotation_lst = [];

        this.unique_locs = [];

        this.selectionframe = null;
        this.active_object_idx = null;

        this.mousedown_active = false;
        
        this.INTERSECTED = null;
      
        // window.requestAnimationFrame( this.render );
    }

    aload_config(){
        $.getJSON(this.config_file, this.load_config);
    }

    /**
     * loads the setting from config.json and eventually updates the GUI elements
     */
    load_config( ){

        // const fs = require('fs');

        //let rawdata = fs.readFileSync(this.config_file);
        //let ConfigData = JSON.parse(rawdata);

        // let ConfigData = require( this.config_file );
        this.ConfigData = ConfigData;

        this.init_long = ConfigData.init_pos.longitude;
        this.init_lat = ConfigData.init_pos.latitude;
        
        this.show_stats = ConfigData.options.show_stats;

        this.value_min = ConfigData.colorbar.min_value;
        this.value_max = ConfigData.colorbar.max_value;
        this.unit = ConfigData.data.unit;

        this.title_text = ConfigData.title;
        this.colorbar_title = ConfigData.colorbar.title;
        this.data_source = ConfigData.data.source;

        // set data information etc. in main document
        parent.document.getElementById("data_url").value = this.data_source;
        parent.document.getElementById("title").value = this.title_text;
        parent.document.getElementById("data_ref").href = this.data_source;
        parent.document.getElementById("data_author").innerHTML = ConfigData.data.author;
        parent.document.getElementById("data_author").href = ConfigData.data.author_link;
        parent.document.getElementById("data_license").innerHTML = ConfigData.data.license;
        parent.document.getElementById("data_license").href = ConfigData.data.license_link;
        parent.document.getElementById("data_comment").innerHTML = ConfigData.data.comment;
        parent.document.getElementById( "chb-showAnnotations" ).checked = ConfigData.options.show_sample_number;
        
        // this.scene.updadate_camera_postion( this.init_long, this.init_lat );

    }

    /**
     * adds a new three.js / WebGL scene
     */
    add_scene(){
        this.scene = new WebGLScene( this.init_long, this.init_lat, 
                                     this.show_stats );
    }

    /**
     * loads data (sample values and maps) asynchronously
     */
    load_data( ){
        // load sample data
        $.getJSON(this.ConfigData.data.source, this.create_data_visualizations);

        // load map from JSON file
        $.getJSON(this.ConfigData.map.contour_lines.filename, this.load_map_contour);
        $.getJSON(this.ConfigData.map.background_fill.filename, this.load_map_background);
    }

    /**
     * creates a look-up-table for value coloring
     */
    create_lut( ){
        var lut = new Lut();
        lut.setColorMap( 'cooltowarm' );
        lut.setMax( this.value_max );
        lut.setMin( this.value_min );
        // Legend code removed after release 103...
        // lut.setLegendOn({layout: "vertical", position: {x: 0, y:0, z:0}, dimensions: {width:0.1, height:1.0}});
        // console.log(lut.Legend);

        this.lut = lut;
    }

    /**
     * Creates the 3D map representations of the measurement samples
     * @param {Object} json - GeoJSON data
     */
    create_data_visualizations(json) {

    	var parsed_data = read_json(json, this.ConfigData);
    	this.unique_locs = parsed_data.unique_locs;
        if (this.unit === null) {
            this.unit = parsed_data.unit;
            this.scene.uiScene.remove(this.colorbar_labels.title);
            var new_colorbar_labels = setLegendLabels(this.lut,
                {
                    fontsize: 10,
                    fontface: "Arial",
                    title: this.colorbar_title,
                    um: this.unit,
                    ticks: 5,
                    decimal: 0,
                    notation: "standard"
                }
            );
            this.scene.uiScene.add(new_colorbar_labels.title);
            this.colorbar_labels = new_colorbar_labels;
        }

    	var visual_objects_dict = create_meshes_for_all_data_points( parsed_data, this.lut, this.ConfigData.options.scale_factor );
    	var mesh_lst = visual_objects_dict.meshes;
    	var annotation_lst = visual_objects_dict.annotations;

    	for ( const mesh of mesh_lst ){
    	    this.scene.add(mesh);
    	    this.mesh_lst.push(mesh);
    	}
    	for ( const annotation of annotation_lst ){
    	    this.scene.add(annotation);
    	    this.annotation_lst.push(annotation);
    	}
        
    	// update document fields
        parent.document.getElementById("info_text").innerHTML = this.title_text + " (showing max. values)";
        
        if (this.ConfigData.options.show_sample_number){
            this.scene.camera.layers.enable(1);
            $("#chb-showAnnotations").prop("checked", true);
        }else{
            this.scene.camera.layers.disable(1);
            $("#chb-showAnnotations").prop("checked", false);
        }
        // console.log("chb-Status:", $("#chb-showAnnotations").is(':checked'));
        
        // update colorbar unit
        
    }


    /**
     * adds a color bar + legend
     */
    add_colorbar( ) {

    	var colorbar_mesh = setLegendOn(this.lut,  
            {
                layout: "vertical",
                position: {x: 0, y:0, z:0},
                dimensions: {width:0.05, height:1.0}
            }
    	);
    	var colorbar_labels = setLegendLabels(this.lut,
            {
                fontsize: 10,
                fontface: "Arial",
                title: this.colorbar_title,
                um: this.unit,
                ticks: 5,
                decimal: 0,
                notation: "standard"
            }
    	);

    	//console.log(colorbar_labels);

    	// display color bar with legend
    	this.scene.uiScene.add(colorbar_mesh);	// adds color bar

    	// add color bar title
    	this.scene.uiScene.add(colorbar_labels.title);

    	//add color bar lines
    	for (const line of Object.keys(colorbar_labels.lines)){
    		this.scene.uiScene.add(colorbar_labels.lines[line]);
    	}

    	//add color bar ticks
    	for (const tick of Object.keys(colorbar_labels.ticks)){
    		this.scene.uiScene.add(colorbar_labels.ticks[tick]);
    	}

        this.colorbar_labels = colorbar_labels;
    	// return uiScene;
    }

    // load back ground area for map (e.g. whole country)
    load_map_background ( json_data ) {

    	var scene_elements = load_map_geojson( json_data, parseInt(this.ConfigData.map.background_fill.color), 2, true);
    	for (const scene_element of scene_elements){
    	    this.scene.add(scene_element);
    	}
    }

    // load map contour lines (e.g. for federal states)
    load_map_contour ( json_data ) {

    	var scene_elements = load_map_geojson( json_data, parseInt(this.ConfigData.map.contour_lines.color),
    	                                       this.ConfigData.map.contour_lines.linewidth);
        for (const scene_element of scene_elements){
            this.scene.add(scene_element);
        }
    }

    animate() {
        // requestAnimationFrame( this.animate );
    	this.scene.controls.update(); // only required if controls.enableDamping = true, or if controls.autoRotate = true
    	this.render();
        /*
        console.log("camera pos:", this.scene.camera.position.x,
                                   this.scene.camera.position.y,
                                   this.scene.camera.position.z);
        */
    }

    render() {

    	var plot_props;
    	// find intersections

    	this.scene.raycaster.setFromCamera( this.scene.mouse, this.scene.camera );

    	var intersects = this.scene.raycaster.intersectObjects( this.mesh_lst );
    	// console.log(mesh_lst);

    	if ( intersects.length > 0 ) {
    		// console.log('No. of intresects', intersects.length);

    		if ( this.INTERSECTED !== intersects[ 0 ].object && !this.mousedown_active) {

    			if ( this.INTERSECTED ) this.INTERSECTED.material.opacity = 1.0;

    			this.INTERSECTED = intersects[ 0 ].object;
    			this.INTERSECTED.material.opacity = 1.0;

    			var mesh_idx = null;
    			// get index of selected object in mesh_lst
    			for (const [index, mesh] of this.mesh_lst.entries()){
    				if (mesh.id === this.INTERSECTED.id){
    					mesh_idx = index;
    					this.active_object_idx = mesh_idx;
    					break;
    				}
    			}

    			if (this.selectionframe !== null){
    				this.scene.scene.remove(this.selectionframe);
    			}

    			// need to rotate the geometry, if this was not created from point data
    			if (this.unique_locs[mesh_idx].point){
    				var edge_selected = new THREE.EdgesGeometry( this.INTERSECTED.geometry );
    				this.selectionframe = new THREE.LineSegments( edge_selected, new THREE.LineBasicMaterial( { color: 0x000000 } ) );
    				this.selectionframe.position.x = this.unique_locs[mesh_idx].center.x;
    				this.selectionframe.position.z = -this.unique_locs[mesh_idx].center.y;
    				this.selectionframe.position.y = 0.01;
    				this.selectionframe.scale.x = this.INTERSECTED.scale.x;
    				this.selectionframe.scale.y = this.INTERSECTED.scale.y;
    				this.selectionframe.scale.z = this.INTERSECTED.scale.z;
    			}else {
    				var edge_selected = new THREE.EdgesGeometry( this.INTERSECTED.geometry );
    				this.selectionframe = new THREE.LineSegments( edge_selected, new THREE.LineBasicMaterial( { color: 0x000000 } ) );
    				this.selectionframe.scale.z = this.INTERSECTED.scale.z;
    				this.selectionframe.translateY(this.selectionframe.position.z + 0.01);
    				this.selectionframe.rotateX(-Math.PI/2);
    			}
    			this.scene.add(this.selectionframe);
    			// console.log('selectionframe', selectionframe);

    			// this is the marker on the bottom...
    			if (this.ConfigData.options.show_marker){
    				this.scene.selection_pointer.visible = true;
    				this.scene.selection_pointer.position.set( this.unique_locs[mesh_idx].center.x, 0.01,
                                    -this.unique_locs[mesh_idx].center.y);
    			}else{
    				this.scene.selection_pointer.visible = false;
    			}

    			// update sample property html elements acc. to selected sample location
                parent.document.getElementById("props_summary").innerHTML = create_location_props_string(
                    this.unique_locs[mesh_idx], this.ConfigData.options.numeric_precision, this.unit
                );
    		parent.document.getElementById("accordion_samples").innerHTML = create_samples_summary_string(
                    this.unique_locs[mesh_idx].sample_properties);
                plot_props = create_histogram_props(this.unique_locs[mesh_idx].sample_properties, 
                    this.ConfigData.colorbar.min_value, this.ConfigData.colorbar.max_value,
                    this.unit);
                var plot = parent.document.getElementById('plot');
                Plotly.newPlot( plot, plot_props.curves, plot_props.layout, plot_props.options);

    		}

    	} else {

    		// if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
    		if ( this.INTERSECTED ) this.INTERSECTED.material.opacity = 1.0;

    		this.INTERSECTED = null;
    		// selection_pointer.visible = false;

    	}

    	this.scene.renderer.render( this.scene.scene, this.scene.camera );
    	this.scene.renderer.render( this.scene.uiScene, this.scene.orthoCamera);
    	this.scene.stats.update();

    }

    // event triggered functions follow here

    // updates the results representation based on display options - max, min or mean
    updateResults( event ){

    	var display_str;

    	if (event === null){
    		var results_type = "max";
    	}else{
    		var results_type = event.detail.datatype;
    	}

    	console.log('Received event for data type', results_type);

    	var old_scale, new_scale, color, active_object_scale;

    	var scale_factor = this.ConfigData.options.scale_factor;

    	for (const [idx, mesh] of this.mesh_lst.entries())
    	{
    		switch(results_type)
    		{
    			case "max":
    				old_scale = mesh.scale.y;
    				new_scale = this.unique_locs[idx].max_value * scale_factor;
    				color = this.lut.getColor( this.unique_locs[idx].max_value );
    				display_str = "max.";
    				break;
    			case "min":
    				new_scale = this.unique_locs[idx].min_value * scale_factor;
    				color = this.lut.getColor( this.unique_locs[idx].min_value );
    				display_str = "min.";
    				break;
    			case "mean":
    				new_scale = this.unique_locs[idx].mean_value * scale_factor;
    				color = this.lut.getColor( this.unique_locs[idx].mean_value );
    				display_str = "mean";
    				break;
    			default:
    				old_scale = mesh.scale.y;
    				new_scale = this.unique_locs[idx].max_value * scale_factor;
    				color = this.lut.getColor( this.unique_locs[idx].max_value );
    				display_str = "max.";
    				break;
    		}
    		if (this.unique_locs[idx].point){	// point data used
    			mesh.scale.y = new_scale;
    		}else {
    			mesh.scale.z = new_scale;
    		}
    		this.annotation_lst[idx].position.y = new_scale + 0.1;
    		this.annotation_lst[idx].updateMatrix();
    		mesh.material.color = color;
    		mesh.updateMatrix();
    		if (idx === this.active_object_idx){
    			var active_object_scale = new_scale;
    		}
    	}

    	// scale selection frame if existing
    	if (this.selectionframe !== null){
    		if (this.unique_locs[this.active_object_idx].point){
    			this.selectionframe.scale.y = active_object_scale;
    		}else {
    			this.selectionframe.scale.z = active_object_scale;
    		}
    	}

    	parent.document.getElementById("info_text").innerHTML = this.title_text + " (showing " + display_str + " values)";
    	console.log('Data rescaled to', results_type);
    }

    // displays or hides annotations (e.g. number of samples per measurement location)
    showAnnotations( event ){

    	var enabled = event.detail.enabled;

    	if (enabled){
    		this.scene.camera.layers.enable(1);
    	}
    	else{
    		this.scene.camera.layers.disable(1);
    	}

    }

    onWindowResize() {

    	this.scene.camera.aspect = window.innerWidth / window.innerHeight;
    	this.scene.camera.updateProjectionMatrix();

    	this.scene.renderer.setSize( window.innerWidth, window.innerHeight );

    }
    
    /** calculate mouse position in normalized device coordinates
    /* (-1 to +1) for both components
     * 
     * @param {object} event - mouse event
     * @returns {undefined}
     */
    onMouseUp( event ) {
    	this.mousedown_active = false;

    	if (event.button === 0){
            // mousedown_active = false;
            event.preventDefault();

            this.scene.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
            this.scene.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
    	}
    }

    onMouseDown( event ) {
    	this.mousedown_active = true;
    }
}

/*
module.exports = {
    GeoJSONViewer: GeoJSONViewer
};
*/