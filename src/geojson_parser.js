/*
This file is part of geoJSciView

Copyright 2020 Thies Hecker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

const THREE = require('three');


/**
 * Read the geoJSON of the data
 * @param {Object} json - GeoJSON data
 * @param {Object} ConfigData - project configutation data
 * @returns {Object} - unique location data and unit (keys 'unique_locs' & 'unit')
 */
function read_json(json, ConfigData) {

    var unit;

	// console.log('Measurement data:', json);
	// get unit - assume this is the same for all data sets...
	if (ConfigData.data.unit == null){
		try {
			unit = json.features[0].properties.unit;
		}
		catch (err) {
			console.log(err.message);
			unit = "";
		}
	}
	else {
		unit = ConfigData.data.unit;
	}

	var mean_x, mean_y, param_value, x_sum, y_sum, count;
	var x_vals, y_vals;

	var display_data = [];

	// extract longitude and latitude and measurement value from data
	display_data = extract_features(json, ConfigData);

	// find unique locations
	var unique_locs = [];
	var update_locs = false;
	console.log('Number of data sets:', display_data.length);

	for (const data_set of display_data){
		update_locs = true;
		var set_longitude = data_set.longitude;
		var set_latitude = data_set.latitude;
		var set_value = data_set.value;
		if (unique_locs.length > 0){
			for (const unique_loc of unique_locs){
				if (data_set.center.x == unique_loc.center.x && data_set.center.y == unique_loc.center.y){	// location already defined in data, new sample
					update_locs = false;
					// update stats
					if (set_value > unique_loc.max_value){
						unique_loc.max_value = set_value;
					}
					else if (set_value < unique_loc.min_value){
						unique_loc.min_value = set_value;
					}
					unique_loc.mean_value = (unique_loc.mean_value * unique_loc.samples + set_value) / (unique_loc.samples + 1);
					unique_loc.samples += 1;
					unique_loc.sample_properties.push(data_set.properties);
					break;
				}
			}
		}
		if (update_locs){
			//TODO: use data_set as base and update with additional properties
			unique_locs.push({
				longitude: set_longitude,
				latitude: set_latitude,
				samples: 1,
				max_value: set_value,
				min_value: set_value,
				mean_value: set_value,
				sample_properties: [data_set.properties],
				point: data_set.point,
				center: data_set.center,
				shape: data_set.shape});
		}
	}
	console.log('Number of unique locations:', unique_locs.length);
	//console.log(unique_locs);

    return {unique_locs: unique_locs, unit: unit};
}


/**
 * Extracts the features of different types from the json data.
 * @param {json} json_data - GeoJSON data
 * @param {json} ConfigData - JSON object, which defines the project configuration options
 */
function extract_features(json_data, ConfigData) {

	var mean_x, mean_y, param_value;

	//check if data is of type FeatureCollection
	if (json_data.type != 'FeatureCollection'){
		console.log('Unsupported data type', json_data.type, '. Only type \'FeatureCollection\' is supported.');
		return;
	}

	var display_data = [];
	var x_vals = [];
	var y_vals = [];
	for (const feature of json_data.features){

		param_value = feature.properties[ConfigData.data.value_name];

		switch(feature.geometry.type) {

			case 'Point':
				mean_x = feature.geometry.coordinates[0];
				mean_y = feature.geometry.coordinates[1];
				display_data.push({
					longitude: mean_x, latitude: mean_y, value: param_value, properties: feature.properties, point: true,
					center: {x: mean_x, y: mean_y}});
				console.log('Found Point at (center):', mean_x, mean_y);
				break;
			case 'Polygon':
				display_data.push(read_polygon({
				    poly_coords: feature.geometry.coordinates,
				    poly_props: feature.properties,
				    convert_to_point: ConfigData.data.convert_to_point}));
				console.log('Found Polygon at (center):', display_data[display_data.length-1].center.x,
					display_data[display_data.length-1].center.y);
				break;
			case 'MultiPoint':
				// Multi Point data will create identical copies for each point in the array
				for (const point_coords of feature.geometry.coordinates){
					mean_x = point_coords[0];
					mean_y = point_coords[1];
					display_data.push({
						longitude: mean_x, latitude: mean_y, value: param_value, properties: feature.properties, point: true,
						center: {x: mean_x, y: mean_y}});
					console.log('Found Point of MultiPoint at (center):', mean_x, mean_y);
				}
				break;
			case 'MultiPolygon':
				for (const polygon of feature.geometry.coordinates){
					display_data.push(read_polygon({
					    poly_coords: polygon,
					    poly_props: feature.properties,
					    convert_to_point: ConfigData.data.convert_to_point}));
					console.log('Found Polygon of MultiPolygon at (center):', display_data[display_data.length-1].center.x,
						display_data[display_data.length-1].center.y);
				}
				break;
			default:
				console.log('Unsupported geometry type', feature.geometry,
					'. Only Point, Polygon, Multipoint and Multipolygon are supported.');
				break;
		}
	}
	//console.log('Display data:', display_data);
	return display_data;
}


/**
 * Reads a polygon including the holes and converts it to THREE.Shape.
 * @param {array} poly_coords - Array of arrays of coordinates for each item (i.e. polygon outline and eventually holes)
 * @param {Object} poly_props - GeoJSON Feature properties
 * @param {boolean} convert_to_point - If true no shape object will be created
 * @returns {Object}
 */
function read_polygon ({poly_coords=null, poly_props=null, convert_to_point=false}) {
	var polygon, mean_x, mean_y;
	var x_vals = [];
	var y_vals = [];
	var outline;
	var holes = [];
	var poly_idx;
	for (poly_idx = 0; poly_idx < poly_coords.length; poly_idx++){
		//Extract hole coordinates to Path...
		var coord_array = [];
		for (const coord of poly_coords[poly_idx]){
			coord_array.push(new THREE.Vector2(coord[0], coord[1]));
			if (poly_idx == 0){
				x_vals.push(coord[0]);
				y_vals.push(coord[1]);
			}
		}
		if (poly_idx == 0){	// this is the outline
			outline = coord_array;
			mean_x = (Math.max(...x_vals) + Math.min(...x_vals))/2;
			mean_y = (Math.max(...y_vals) + Math.min(...y_vals))/2;
		}
		else {	// everything else is a hole
			holes.push(new THREE.Path(coord_array));
			console.log('Found hole for shape at:', mean_x, mean_y);
		}
	}

	var shape = new THREE.Shape( outline );
	shape.holes = holes;

	if (convert_to_point){
		polygon = {
			longitude: mean_x, latitude: mean_y, value: poly_props.value, properties: poly_props, point: true,
			center: {x: mean_x, y: mean_y}, shape: null};
	}else{
		polygon = {
			longitude: x_vals, latitude: y_vals, value: poly_props.value, properties: poly_props, point: false,
			center: {x: mean_x, y: mean_y}, shape: shape};
	}

	return polygon;
}

module.exports = {
    read_json: read_json,
    read_polygon: read_polygon,
    extract_features: extract_features,
};
