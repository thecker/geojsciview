/**
 * prepares the histogram for the current data set
 * @param {array} location_sample_properties - list of GeoJSON feature properties for a sample location
 * @param {float} value_min - min. value for bin range
 * @param {float} value_max - max. value for bin range
 * @param {string} unit - Unit string
 * @param {int} bins - Number of bins for histogram
 */
export function create_histogram_props ( location_sample_properties, value_min, value_max, unit, bins=10 ){
	var values = [];
	for ( const sample_props of location_sample_properties){
		values.push(sample_props.value);
	}

	// var plot = parent.document.getElementById('plot');

	var curve1 = {
		type: 'histogram',
		x: values,
		marker: {
	    	color: "#EEEEEE",
	     	line: {
	      	color:  "#CCCCCC",
	      	width: 1
	    	}
	   },
		xbins: {
    		end: value_max,
    		size: (value_max - value_min)/bins,
    		start: value_min
    	}
	};

	var layout = {
		// margin: { t: 80, r:40, l:50, b:40 },
		title: {text: "Histogram of measurements"},
		xaxis: {title: unit, range: [value_min, value_max], ticks: "inside"},
  		yaxis: {title: "Count", tick0: value_min, dtick: 1, showline: false}
	};

	// interactive display is causing some weird issues... therefore use static
	var options = {staticPlot: true};

    return {curves: [curve1], layout: layout, options: options}
	// Plotly.newPlot( plot, [curve1], layout, options );
}

/**
 * creates a html string for the sample summary
 * @param {array} location_sample_properties - list of GeoJSON feature properties for a sample location
 */
export function create_samples_summary_string ( location_sample_properties ){
	var html_str = "";
	var index = 1;
	var str_value

	var no_samples = location_sample_properties.length;

	for (const sample_props of location_sample_properties){
		html_str = html_str + "<h3>Sample " + index.toString() + "/" + no_samples.toString() + "</h3><div><p>";
		index += 1;
		for (const key of Object.keys(sample_props)){
			if (sample_props[key] != null){
				str_value = sample_props[key].toString();
			}
			else{
				str_value = "unknown";
			}
			html_str = html_str + key + ": " + str_value + "<br>";
		}
		html_str = html_str + "</p></div>";
	}
	return html_str;
}

/**
 * create html string for the sample location (e.g. longitude, latitude, min./max. sample values)
 */
export function create_location_props_string ( summary_dict, precision, unit ){

	var html_str = "";

	var longitude_str = parseFloat(summary_dict.center.x.toPrecision(precision));
	var latitude_str = parseFloat(summary_dict.center.y.toPrecision(precision));
	var maxval_str = parseFloat(summary_dict.max_value.toPrecision(precision));
	var minval_str = parseFloat(summary_dict.min_value.toPrecision(precision));
	var meanval_str = parseFloat(summary_dict.mean_value.toPrecision(precision));

	html_str = html_str + "<span>longitude: </span><span>" + longitude_str + " [°]</span>";
	html_str = html_str + "<span>latitude: </span><span>" + latitude_str + " [°]</span>";
	html_str = html_str + "<span>samples: </span><span>" + summary_dict.samples.toString() + "</span>";
	html_str = html_str + "<span>max. value: </span><span>" + maxval_str + " [" + unit + "]</span>";
	html_str = html_str + "<span>min. value: </span><span>" + minval_str + " [" + unit + "]</span>";
	html_str = html_str + "<span>mean value: </span><span>" + meanval_str + " [" + unit + "]</span>";

	return html_str;
}