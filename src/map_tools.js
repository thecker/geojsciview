/*
This file is part of geoJSciView

Copyright 2020 Thies Hecker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

const THREE = require('three');

/**
 * Loads map defined in GeoJSON data into three.js geometry objects.
 * @param {Object} json_data - GeoJSON contour data
 * @param {int} color - Color value (as hex value - e.g. 0xCCCCCC)
 * @param {int} line_width - Width of the lines in pixels
 * @param {boolean} solid - If true this is a filled area, if false just a contour
 */
function load_map_geojson ( json_data, color, line_width, solid=false ) {

	//console.log(json_data);

	var scene_elements = [];

	var material = new THREE.LineBasicMaterial( {color: color, linewidth: line_width });
	var shape_material = new THREE.MeshBasicMaterial( { color: color } );

	for (const [feat_idx, feature] of json_data.features.entries()){

		switch(feature.geometry.type) {
			case 'Polygon': {
				var points = [];
				var points2d = [];
				for (const [coord_idx, coord] of feature.geometry.coordinates[0].entries()) {
					if (isNaN(coord[0]) || isNaN(coord[1])) {
						console.log('Encountered NaN in feature:', feat_idx, 'coord:', coord_idx);
					}
					else{
						points.push(new THREE.Vector3(coord[0], 0, -coord[1]));
						points2d.push(new THREE.Vector2(coord[0], coord[1]));
					}
				}
				if (!solid) {
					var buf_geometry = new THREE.BufferGeometry().setFromPoints(points);
					var line = new THREE.Line( buf_geometry, material );
					// scene.add(line);
					scene_elements.push(line);
					// console.log('Map line added to scene elements.')
				}else {
					var shape = new THREE.Shape(points2d);
					var shape_geometry = new THREE.ShapeGeometry(shape);
					var mesh = new THREE.Mesh( shape_geometry, shape_material ) ;
					mesh.rotateX(-Math.PI/2);
					// scene.add( mesh );
					scene_elements.push(mesh);
					// console.log('Map shape added to scene elements.')
				}
				break;
			}
			case 'MultiPolygon': {
				//console.log('MultiPolygon');
				for (const [poly_idx, polygon] of feature.geometry.coordinates.entries()){
					var points = [];
					var points2d = [];
					for (const [coord_idx, coord] of polygon[0].entries()) {
						if (isNaN(coord[0]) || isNaN(coord[1])) {
							console.log('Encountered NaN in feature:', feat_idx, ', polygon:', poly_idx, 'coord:', coord_idx);
						}
						else{
							points.push(new THREE.Vector3(coord[0], 0, -coord[1]));
							points2d.push(new THREE.Vector2(coord[0], coord[1]));
						}
					}
					if (!solid) {
						var buf_geometry = new THREE.BufferGeometry().setFromPoints(points);
						var line = new THREE.Line( buf_geometry, material );
						// scene.add(line);
						scene_elements.push(line);
						// console.log('Map line added to scene elements.')
					}else {
						var shape = new THREE.Shape(points2d);
						var shape_geometry = new THREE.ShapeGeometry(shape);
						var mesh = new THREE.Mesh( shape_geometry, shape_material ) ;
						mesh.rotateX(-Math.PI/2);
						// scene.add( mesh );
						scene_elements.push(mesh);
						// console.log('Map shape added to scene elements.')
					}
				}
				break;
			}
		}
	}
	console.log(scene_elements.length, ' elements extracted from map.');
	return scene_elements;
}

module.exports = {
    load_map_geojson: load_map_geojson
};
