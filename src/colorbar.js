/*
This file is part of geoJSciView

Copyright 2020 Thies Hecker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

See original disclaimer of Lut.js/three.js r103 below:

**
* @author daron1337 / http://daron1337.github.io/
*

The MIT License

Copyright © 2010-2019 three.js authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
Currently only vertical bar code has been fixed.

TODO: fix code for horizontal bar
*/
import * as THREE from 'three';

// take a THREE.js Lut object and parameters as defined for r103
export function setLegendOn ( lut, parameters ) {

		if ( parameters === undefined ) {

			parameters = {};

		}

		lut.legend = {};

		lut.legend.layout = parameters.hasOwnProperty( 'layout' ) ? parameters[ 'layout' ] : 'vertical';

		lut.legend.position = parameters.hasOwnProperty( 'position' ) ? parameters[ 'position' ] : { 'x': 4, 'y': 0, 'z': 0 };

		lut.legend.dimensions = parameters.hasOwnProperty( 'dimensions' ) ? parameters[ 'dimensions' ] : { 'width': 0.5, 'height': 3 };

		lut.legend.canvas = document.createElement( 'canvas' );

		lut.legend.canvas.setAttribute( 'id', 'legend' );
		lut.legend.canvas.setAttribute( 'hidden', true );

		document.body.appendChild( lut.legend.canvas );

		lut.legend.ctx = lut.legend.canvas.getContext( '2d' );

		lut.legend.canvas.setAttribute( 'width', 1 );
		lut.legend.canvas.setAttribute( 'height', lut.n );

		lut.legend.texture = new THREE.Texture( lut.legend.canvas );

		var imageData = lut.legend.ctx.getImageData( 0, 0, 1, lut.n );

		var data = imageData.data;

		// lut.map = THREE.ColorMapKeywords[ lut.mapname ];

		var k = 0;

		var step = 1.0 / lut.n;

		for ( var i = 1; i >= 0; i -= step ) {

			for ( var j = lut.map.length - 1; j >= 0; j -- ) {

				if ( i < lut.map[ j ][ 0 ] && i >= lut.map[ j - 1 ][ 0 ] ) {

					var min = lut.map[ j - 1 ][ 0 ];
					var max = lut.map[ j ][ 0 ];

					var minColor = new THREE.Color( 0xffffff ).setHex( lut.map[ j - 1 ][ 1 ] );
					var maxColor = new THREE.Color( 0xffffff ).setHex( lut.map[ j ][ 1 ] );

					var color = minColor.lerp( maxColor, ( i - min ) / ( max - min ) );

					data[ k * 4 ] = Math.round( color.r * 255 );
					data[ k * 4 + 1 ] = Math.round( color.g * 255 );
					data[ k * 4 + 2 ] = Math.round( color.b * 255 );
					data[ k * 4 + 3 ] = 255;

					k += 1;

				}

			}

		}

		lut.legend.ctx.putImageData( imageData, 0, 0 );
		lut.legend.texture.needsUpdate = true;

		lut.legend.legendGeometry = new THREE.PlaneBufferGeometry( lut.legend.dimensions.width, lut.legend.dimensions.height );
		lut.legend.legendMaterial = new THREE.MeshBasicMaterial( { map: lut.legend.texture, side: THREE.DoubleSide } );

		lut.legend.mesh = new THREE.Mesh( lut.legend.legendGeometry, lut.legend.legendMaterial );

		if ( lut.legend.layout == 'horizontal' ) {

			lut.legend.mesh.rotation.z = - 90 * ( Math.PI / 180 );

		}

		lut.legend.mesh.position.copy( lut.legend.position );

		return lut.legend.mesh;

};

function setLegendOff (lut) {

		lut.legend = null;

		return lut.legend;

};

function setLegendLayout ( lut, layout ) {

		if ( ! lut.legend ) {

			return false;

		}

		if ( lut.legend.layout == layout ) {

			return false;

		}

		if ( layout != 'horizontal' && layout != 'vertical' ) {

			return false;

		}

		lut.layout = layout;

		if ( layout == 'horizontal' ) {

			lut.legend.mesh.rotation.z = 90 * ( Math.PI / 180 );

		}

		if ( layout == 'vertical' ) {

			lut.legend.mesh.rotation.z = - 90 * ( Math.PI / 180 );

		}

		return lut.legend.mesh;

};

function setLegendPosition (lut, position ) {

		lut.legend.position = new THREE.Vector3( position.x, position.y, position.z );

		return lut.legend;

};

export function setLegendLabels ( lut, parameters, callback ) {

		if ( ! lut.legend ) {

			return false;

		}

		if ( typeof parameters === 'function' ) {

			callback = parameters;

		}

		if ( parameters === undefined ) {

			parameters = {};

		}

		lut.legend.labels = {};

		lut.legend.labels.fontsize = parameters.hasOwnProperty( 'fontsize' ) ? parameters[ 'fontsize' ] : 24;

		lut.legend.labels.fontface = parameters.hasOwnProperty( 'fontface' ) ? parameters[ 'fontface' ] : 'Arial';

		lut.legend.labels.title = parameters.hasOwnProperty( 'title' ) ? parameters[ 'title' ] : '';

		lut.legend.labels.um = parameters.hasOwnProperty( 'um' ) ? ' [ ' + parameters[ 'um' ] + ' ]' : '';

		lut.legend.labels.ticks = parameters.hasOwnProperty( 'ticks' ) ? parameters[ 'ticks' ] : 0;

		lut.legend.labels.decimal = parameters.hasOwnProperty( 'decimal' ) ? parameters[ 'decimal' ] : 2;

		lut.legend.labels.notation = parameters.hasOwnProperty( 'notation' ) ? parameters[ 'notation' ] : 'standard';

		var backgroundColor = { r: 255, g: 100, b: 100, a: 0.8 };
		var borderColor = { r: 255, g: 0, b: 0, a: 1.0 };
		var borderThickness = 4;

		var canvasTitle = document.createElement( 'canvas' );
		var contextTitle = canvasTitle.getContext( '2d' );
		
		contextTitle.textAlign = "center";
		contextTitle.textBaseline = "bottom";
		contextTitle.translate(canvasTitle.width/2, canvasTitle.height/2);
		contextTitle.rotate(-90 * Math.PI / 180);

		contextTitle.font = 'Normal ' + lut.legend.labels.fontsize * 2 + 'px ' + lut.legend.labels.fontface;
		contextTitle.fillStyle = 'rgba(' + backgroundColor.r + ',' + backgroundColor.g + ',' + backgroundColor.b + ',' + backgroundColor.a + ')';
		contextTitle.strokeStyle = 'rgba(' + borderColor.r + ',' + borderColor.g + ',' + borderColor.b + ',' + borderColor.a + ')';
		// contextTitle.lineWidth = borderThickness;
		contextTitle.fillStyle = 'rgba( 0, 0, 0, 1.0 )';
		
		// get canvas size and resize canvas
		var req_canvas_width = contextTitle.measureText(lut.legend.labels.title.toString() + lut.legend.labels.um.toString()).width
		canvasTitle.height = req_canvas_width;
		
		// reinit parameters
		contextTitle.textAlign = "center";
		contextTitle.textBaseline = "bottom";
		contextTitle.translate(canvasTitle.width/2, canvasTitle.height/2);
		contextTitle.rotate(-90 * Math.PI / 180);
		contextTitle.font = 'Normal ' + lut.legend.labels.fontsize * 2 + 'px ' + lut.legend.labels.fontface;
		contextTitle.fillStyle = 'rgba(' + backgroundColor.r + ',' + backgroundColor.g + ',' + backgroundColor.b + ',' + backgroundColor.a + ')';
		contextTitle.strokeStyle = 'rgba(' + borderColor.r + ',' + borderColor.g + ',' + borderColor.b + ',' + borderColor.a + ')';
		// contextTitle.lineWidth = borderThickness;
		contextTitle.fillStyle = 'rgba( 0, 0, 0, 1.0 )';

		contextTitle.fillText( lut.legend.labels.title.toString() + lut.legend.labels.um.toString(), 0, 0);

		var txtTitle = new THREE.CanvasTexture( canvasTitle );
		txtTitle.minFilter = THREE.LinearFilter;

		var spriteMaterialTitle = new THREE.SpriteMaterial( { map: txtTitle } );

		var spriteTitle = new THREE.Sprite( spriteMaterialTitle );

		spriteTitle.scale.set( 0.5, req_canvas_width / 300, 1.0 );

		if ( lut.legend.layout == 'vertical' ) {

			// spriteTitle.position.set( lut.legend.position.x + lut.legend.dimensions.width, lut.legend.position.y + ( lut.legend.dimensions.height * 0.45 ), lut.legend.position.z );
			spriteTitle.position.set( lut.legend.position.x - 0.5 * lut.legend.dimensions.width, lut.legend.position.y, lut.legend.position.z );

		}

		if ( lut.legend.layout == 'horizontal' ) {

			spriteTitle.position.set( lut.legend.position.x * 1.015, lut.legend.position.y + ( lut.legend.dimensions.height * 0.03 ), lut.legend.position.z );

		}

		if ( lut.legend.labels.ticks > 0 ) {

			var ticks = {};
			var lines = {};

			if ( lut.legend.layout == 'vertical' ) {

				var topPositionY = lut.legend.position.y + ( lut.legend.dimensions.height * 0.36 );
				var bottomPositionY = lut.legend.position.y - ( lut.legend.dimensions.height * 0.61 );

			}

			if ( lut.legend.layout == 'horizontal' ) {

				var topPositionX = lut.legend.position.x + ( lut.legend.dimensions.height * 0.75 );
				var bottomPositionX = lut.legend.position.x - ( lut.legend.dimensions.width * 1.2 );

			}

			for ( var i = 0; i < lut.legend.labels.ticks; i ++ ) {

				var value = ( lut.maxV - lut.minV ) / ( lut.legend.labels.ticks - 1 ) * i + lut.minV;

				if ( callback ) {

					value = callback( value );

				} else {

					if ( lut.legend.labels.notation == 'scientific' ) {

						value = value.toExponential( lut.legend.labels.decimal );

					} else {

						value = value.toFixed( lut.legend.labels.decimal );

					}

				}

				var canvasTick = document.createElement( 'canvas' );
				var contextTick = canvasTick.getContext( '2d' );
				
				//canvasTick.height = 64;
				//canvasTick.width = 64;
				
				contextTick.textAlign = "left";
				contextTick.textBaseline = "middle";

				contextTick.font = 'Normal ' + 2 * lut.legend.labels.fontsize + 'px ' + lut.legend.labels.fontface;

				contextTick.fillStyle = 'rgba(' + backgroundColor.r + ',' + backgroundColor.g + ',' + backgroundColor.b + ',' + backgroundColor.a + ')';

				contextTick.strokeStyle = 'rgba(' + borderColor.r + ',' + borderColor.g + ',' + borderColor.b + ',' + borderColor.a + ')';

				contextTick.lineWidth = borderThickness;

				contextTick.fillStyle = 'rgba( 0, 0, 0, 1.0 )';

				// contextTick.fillText( value.toString(), borderThickness, lut.legend.labels.fontsize + borderThickness );
				contextTick.fillText(value.toString(), canvasTick.width/2, canvasTick.height/2)

				var txtTick = new THREE.CanvasTexture( canvasTick );
				txtTick.minFilter = THREE.LinearFilter;

				var spriteMaterialTick = new THREE.SpriteMaterial( { map: txtTick } );

				var spriteTick = new THREE.Sprite( spriteMaterialTick );

				spriteTick.scale.set( 0.5, 0.5, 1.0 );

				if ( lut.legend.layout == 'vertical' ) {
					
					var linePosition = ( lut.legend.position.y - ( lut.legend.dimensions.height * 0.5 ) + 0.01 ) + ( lut.legend.dimensions.height ) * ( ( value - lut.minV ) / ( lut.maxV - lut.minV ) * 0.99 );

					// var position = bottomPositionY + ( topPositionY - bottomPositionY ) * ( ( value - lut.minV ) / ( lut.maxV - lut.minV ) );
					
					spriteTick.position.set( lut.legend.position.x + lut.legend.dimensions.width * 0.8, linePosition, lut.legend.position.z );

					// spriteTick.position.set( lut.legend.position.x + ( lut.legend.dimensions.width * 2.7 ), position, lut.legend.position.z );

				}

				if ( lut.legend.layout == 'horizontal' ) {

					var position = bottomPositionX + ( topPositionX - bottomPositionX ) * ( ( value - lut.minV ) / ( lut.maxV - lut.minV ) );

					if ( lut.legend.labels.ticks > 5 ) {

						if ( i % 2 === 0 ) {

							var offset = 1.7;

						} else {

							var offset = 2.1;

						}

					} else {

						var offset = 1.7;

					}

					spriteTick.position.set( position, lut.legend.position.y - lut.legend.dimensions.width * offset, lut.legend.position.z );

				}

				var material = new THREE.LineBasicMaterial( { color: 0x000000, linewidth: 2 } );

				var points = [];


				if ( lut.legend.layout == 'vertical' ) {

					var linePosition = ( lut.legend.position.y - ( lut.legend.dimensions.height * 0.5 ) + 0.01 ) + ( lut.legend.dimensions.height ) * ( ( value - lut.minV ) / ( lut.maxV - lut.minV ) * 0.99 );

					points.push( new THREE.Vector3( lut.legend.position.x + lut.legend.dimensions.width * 0.55, linePosition, lut.legend.position.z ) );

					points.push( new THREE.Vector3( lut.legend.position.x + lut.legend.dimensions.width * 0.7, linePosition, lut.legend.position.z ) );

				}

				if ( lut.legend.layout == 'horizontal' ) {

					var linePosition = ( lut.legend.position.x - ( lut.legend.dimensions.height * 0.5 ) + 0.01 ) + ( lut.legend.dimensions.height ) * ( ( value - lut.minV ) / ( lut.maxV - lut.minV ) * 0.99 );

					points.push( new THREE.Vector3( linePosition, lut.legend.position.y - lut.legend.dimensions.width * 0.55, lut.legend.position.z ) );

					points.push( new THREE.Vector3( linePosition, lut.legend.position.y - lut.legend.dimensions.width * 0.7, lut.legend.position.z ) );

				}

				var geometry = new THREE.BufferGeometry().setFromPoints( points );

				var line = new THREE.Line( geometry, material );

				lines[ i ] = line;
				ticks[ i ] = spriteTick;

			}

		}

		return { 'title': spriteTitle, 'ticks': ticks, 'lines': lines };

	}
