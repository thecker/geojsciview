/*
This file is part of geoJSciView

Copyright 2020 Thies Hecker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

import * as THREE from 'three';

import {GUI} from 'three/examples/jsm/libs/dat.gui.module';

import {MapControls} from 'three/examples/jsm/controls/OrbitControls';

import {Lut} from 'three/examples/jsm/math/Lut';

import Stats from 'three/examples/jsm/libs/stats.module';

/**
 * creates up the three.js scene (including camera, lights,..)
 **/
export class WebGLScene {
    constructor( init_long, init_lat, show_stats ) {
        this.camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 100 );

        this.camera.position.set( init_long, 5, -init_lat );
        this.camera.layers.enable( 0 ); // default layer
        this.camera.layers.enable( 1 );	// layer for annotations

        // camera for color bar
        this.orthoCamera = new THREE.OrthographicCamera( - 1, 1, 1, - 1, 1, 2 );
        this.orthoCamera.position.set( 0.9, 0, 1 );
        
        this.uiScene = new THREE.Scene();

        // raycaster for mouse selection
        this.raycaster = new THREE.Raycaster();
        this.mouse = new THREE.Vector2(1, 1);

        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color( 0xFFFFFF );
        this.scene.fog = new THREE.FogExp2( 0xcccccc, 0.002 );

        this.renderer = new THREE.WebGLRenderer( { antialias: true } );
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.autoClear = false;
        this.renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild( this.renderer.domElement );

        // controls

        this.controls = new MapControls( this.camera, this.renderer.domElement );

        //controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)

        this.controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        this.controls.dampingFactor = 0.1;

        this.controls.screenSpacePanning = false;

        this.controls.minDistance = 1;
        this.controls.maxDistance = 500;

        //controls.PolarAngle = Math.PI / 4;

        this.controls.maxPolarAngle = Math.PI/2;
        this.controls.minPolarAngle = 0;
        this.controls.maxAzimuthAngle = 0;
        this.controls.minAzimuthAngle = 0;
        this.controls.target = new THREE.Vector3( init_long, 0, -(init_lat - 3));

        //console.log(controls.getPolarAngle())

        // add selection pointer object
        var curve = new THREE.EllipseCurve(
            0,  0,            // ax, aY
            0.15, 0.15,       // xRadius, yRadius
            0,  2 * Math.PI,  // aStartAngle, aEndAngle
            false,            // aClockwise
            0                 // aRotation
        );
        var points = curve.getPoints( 50 );
        var geometry = new THREE.BufferGeometry().setFromPoints( points );
        var material = new THREE.LineBasicMaterial( { color : 0xff0000, linewidth: 3 } );

        // Create the final object to add to the scene
        this.selection_pointer = new THREE.Line( geometry, material );
        this.selection_pointer.visibile = false;
        this.selection_pointer.rotateX(Math.PI/2);
        this.scene.add(this.selection_pointer);

        // lights
        var light = new THREE.AmbientLight( 0x222222, 7 );
        light.layers.enable( 0 );
        light.layers.enable( 1 );

        // scene.add( light );

        // add statistics (frame rate,...()
        this.stats = new Stats();
        if (show_stats){
            document.body.appendChild( this.stats.dom );
        }

        // GUI for controls
        /*var gui = new GUI();
        gui.add( controls, 'screenSpacePanning' );*/

    }
    /**
     * Adds an element to the WebGLScene
     * @param {object} scene_element - A scene element (e.g. a mesh) 
     */
    add( scene_element ){
        this.scene.add(scene_element);
    }
}
