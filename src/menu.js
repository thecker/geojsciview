/*
This file is part of geoJSciView

Copyright 2020 Thies Hecker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

var $ = require('jquery');
require('webpack-jquery-ui');
require('webpack-jquery-ui/css');

  $( function() {
    $( "#accordion" ).accordion({
      heightStyle: "fill",
      collapsible: true
    });
    
    $( "input:radio" ).checkboxradio();
    $( "#selectdatatype" ).controlgroup();
    
    $( "#selectdatatype" ).on("change", getDataType);
    $( "#chb-showAnnotations" ).on("change", toggleAnnotations);
    
    $( "#update-display" ).button();
    
	 var datatype, show_annot;  
	    
    function getDataType (event) {
    	 // console.log(event);
		 datatype = $( "[name='datatype']:checked" ).val();
		 console.log(datatype);
		 var change_event = new CustomEvent('datatypechange', {detail: {datatype: datatype }});
		 console.log($("iframe"));
		 $("iframe")[0].contentWindow.dispatchEvent(change_event);
		 // console.log(change_event);
    }
    
    function toggleAnnotations(event) {
    	var checked = event.target.checked;
    	var change_event = new CustomEvent('annotationstoggle', {detail: {enabled: checked }});
    	$("iframe")[0].contentWindow.dispatchEvent(change_event);
	 	// console.log(event);
    }
    
/*    $( "#accordion_samples" ).accordion({
    	heightStyle: "content",
    	collapsible: true
    	})*/
    	
/*    $( "#accordion-resizer" ).resizable({
      minHeight: 140,
      minWidth: 200,
      resize: function() {
        $( "#accordion" ).accordion( "refresh" );
      }
    });*/
/*    
    $( "#menu-drag" ).draggable();*/
  } );
