module.exports = {
  moduleFileExtensions: ["js", "json", "jsx", "ts", "tsx", "json"],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
    '^.+\\.(js|jsx)?$': 'babel-jest'
  },
  testEnvironment: 'jsdom',
  moduleNameMapper: {
      '^@/(.*)$': '<rootDir>/src/$1'
  },
  testMatch: [
      '<rootDir>/**/*.test.(js|jsx|ts|tsx)', '<rootDir>/(tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx))'
  ],
  transformIgnorePatterns: [],
  setupFiles: [
    "./__mocks__/document.js", "./__mocks__/webgl_scene.js", "./__mocks__/Plotly.js"
  ]
}