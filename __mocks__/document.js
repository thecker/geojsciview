import { JSDOM } from "jsdom"

const dom = new JSDOM();
global.document = dom.window.document;
global.window = dom.window;
// global.document.createElement('data_url');

global.document.body.innerHTML =
    '<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>' +
    '<div>' +
    '  <span id="data_url" />' +
    '  <span id="title" />' +
    '  <span id="data_ref" />' +
    '  <span id="data_author" />' +
    '</div><div>' +
    '  <span id="data_license" />' +
    '</div><div>' +
    '  <span id="data_comment" />' +
    '</div><div>' +
    '  <span id="chb-showAnnotations" />' +
    '</div><div>' +
    '  <span id="info_text" />' +
    '</div><div>' +
    '  <span id="props_summary" />' +
    '</div><div>' +
    '  <span id="accordion_samples" />' +
    '</div>';
// parent.window = dom.window

global.window.innerWidth = 500;
global.window.innerHeight = 500;

// use the mock for Plotly (which is otherwise loaded from CDN in index.html)
global.Plotly = require('./Plotly.js');
