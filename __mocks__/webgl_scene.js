'use strict';

import * as THREE from 'three';

class WebGLScene {
    constructor( init_long, init_lat ) {
        this.init_long = init_long;
        this.init_lat = init_lat;
        this.scene = [];
        this.camera = new Camera();
        this.renderer = new Renderer();
        this.mouse = {x: 0.0, y: 0.0};
        this.raycaster = new Raycaster();
        this.stats = new Stats();
        this.selection_pointer = new Mesh();
        this.uiScene = new THREE.Scene();
    }
    add( scene_element ){
        this.scene.push(scene_element);
    }
}

class Camera {
    constructor () {
        this.aspect = 0.0;
        this.layers = new Layers();
    }
    updateProjectionMatrix(){
    }
}

class Layers {
    enable( number ){}
    disable ( number ){}
}

class Renderer {
    setSize( width, height ){}
    render(){}
}

class Raycaster {
    constructor(){
        this.intersectObject = [];
    }
    setFromCamera ( mouse, camera ){}
    intersectObjects ( meshes ){
        return this.intersectObject;
    }
    /**
     * Use this for mocking
     * @param {object} object - object to define as intersect return
     * @returns {undefined}
     */
    __setIntersectObject ( object ){
        this.intersectObject = [{object: object}];
    }
}

class Stats {
    update(){}
}

class Mesh {
    constructor(){
        this.visible = true;
        this.position = new Position();
    }
}

class Position {
    set (x, y, z){}
}

module.exports = {
    WebGLScene: WebGLScene
};
